﻿var imgs = {
    //play: './assets/images/play.png',
    logo: './assets/images/logo2.png',
    indexBg: './assets/images/index-bg1.jpg',
    introduceBg: './assets/images/introduce-bg.jpg',
    detailBg: './assets/images/detail-bg.jpg',
    teamBg: './assets/images/team-bg1.jpg',
    introTitle: './assets/images/intro-title.png',
    // friendTitle: './assets/images/friend-title.jpg',
    //detailTitle: './assets/images/detail-title.png',
    // detailTitle:'',
    // detailTitle1:'',
    // detailTitle2:'',
    //detailTitle1: './assets/images/detail-title2.png',
    //detailTitle2: './assets/images/detail-title3.png',
    entry: './assets/images/entry.png',
    arrow: './assets/images/arrow.png',
    //like: './assets/images/like.png',
    point: './assets/images/point.png',
    back: './assets/images/back.png',
    // index: './assets/images/index.png',
    //videoPoster: "./assets/images/video-poster.png",
    //mask: './assets/images/mask.png',

    msg: './assets/images/msg1.png',
    // title1: './assets/images/title1-2.png',
    //zlm: './assets/images/c11.png',
    //zlm1: './assets/images/onWork.png',
    //zlm2: './assets/images/outPic.png',
    // head1: './assets/images/head2.jpg',
    // head2: './assets/images/head1.jpg',
    //heart: './assets/images/heart.jpg',

    head: './assets/images/head.png',
    //edit: './assets/images/edit.png',

    c11: './assets/images/c11.jpg',
    c12: './assets/images/c12.jpg',
    c21: './assets/images/c21.jpg',
    c22: './assets/images/c22.jpg',
    c31: './assets/images/c31.jpg',
    c32: './assets/images/c32.jpg',
    home: './assets/images/home.png'
}

var texts = {
    zlmIntro: '2018年12月28日，中央宣传部向全社会宣传发布郝井文的先进事迹，授予他“时代楷模”称号。',
    zlmName: '郝井文',
    zlmIntroContent: [
        '郝井文是空军航空兵某旅旅长，空军特级飞行员。入伍24年来，他始终牢记职责使命，心系祖国蓝天，苦练精飞、矢志打赢，成长为一名精英飞行员和优秀指挥员。',
        '党的十八大以来，郝井文聚精会神抓备战谋打赢，紧盯对手深研制胜之法，紧跟任务提升指挥素养，紧贴实战培养飞行人才，带领部队出色完成钓鱼岛空中维权、东海防空识别区常态化管控等重大任务，在维护国家主权、安全和发展利益上作出重要贡献，被表彰为“全军爱军精武标兵”，荣获空军首届对抗空战比武“金头盔”，荣立二等功1次、三等功5次。'
    ],

}

var aigangjingye = { //敢于担当
    // title: imgs.detailTitle,
    videoPoster: '',
    videoText: '',
    content: [
        { img: imgs.c11, title: "郝井文驾机参加对抗空战演练（2018年7月20日摄）。新华社发（黄振伟 摄）" },
        { text: "为适应现代战争的形态变化，上世纪90年代起，人民空军开始了换羽重生之路。1994年入伍的郝井文，恰逢其时。" },
        { text: "2001年，郝井文受命改装先进战机。装备的更新换代，对这个跨世纪成长起来的年轻飞行员，无疑是一个重要的转折和一次质的蜕变。" },
        { text: "“2008年，我们就开始融入到体系当中去。”从那时起，郝井文意识到，除了对装备挖潜，未来的信息化战争更是“体系”的较量。" },
        { text: "郝井文真正对“体系”刻骨铭心的认识，是在“红剑-2014”演习中的一次败北经历。" },

        { text: "那天夜晚，为达成作战意图，郝井文决定兵分三路：安排精兵强将，借助地形优势从左右两路突防；自己带一路从中路突击。然而，让信心满满的郝井文始料未及的是，左右两路主攻力量竟在对手的地面防空火力下“全军覆灭”，而面对最密集拦截火力的中路两架飞机，却在干扰机的伴随掩护下成功突防！" },
        { text: "“是干扰机把这两架飞机送进去的！”郝井文彻夜难眠，用从未有过的“战损”代价，换来了对“体系”的沉重认识——“在机械化思维模式下，单兵作战能力就算达到了很高水平，但在体系里可能连施展拳脚的机会都没有。”" },
        { img: imgs.c12, title: "郝井文（右）给新飞行员复盘讲解飞行训练过程（2018年6月23日摄）。新华社发（李吉光 摄）" },
        { text: "2016年，郝井文带领团队与某科研机构协作开发新型战术兵棋推演系统，构建信息指挥、网上对抗、模拟仿真等多种训练手段，实现了利用信息化手段推演信息化作战。" },
        { text: "旅成立以来，他率领部队主动与其他军种部队建立联训机制——先后与陆航旅展开近距火力支援协同演练，与海军驱逐舰支队在远海展开空海攻防训练，与地导、电抗部队和异型机进行对抗演练，积累了大量有价值的一手数据。" },
        { text: "如何不断提升精确制导炸弹命中率？郝井文协调试用工业部门研制的制导炸弹半实物仿真模拟器，反复调试推演，小目标实弹命中精度明显提高。" },
        { text: "目前，这个旅飞行员100%保持最低气象条件起降水平，100%具备低空超低空突防突击能力。在空军“金飞镖-2018”突防突击竞赛考核中，他们发射的6枚导弹全部精准命中目标，夺得团体第一名、包揽个人前两名。" },
    ]
}
var yongyuchuangxin = { //善于作为
    //title: imgs.detailTitle1,

    videoPoster: '',
    videoText: '',
    content: [
        { text: "郝井文所在部队的前身，是当年两次入朝作战、创下辉煌战绩的英雄部队，涌现出了王海、孙生禄、焦景文等战斗英雄和“王海大队”等英雄集体。" },
        { img: imgs.c21, title: "郝井文训练归来（2018年6月23日摄）。新华社发（李吉光 摄）" },
        { text: "“作为英雄团队传人，如何不忘初心、为党奋飞，打造新时代‘空中王牌’？”郝井文时常这样问官兵、问自己。" },
        { text: "“怎么做到？那就是紧贴实战，不断地自我否定！”郝井文说。" },
        { text: "2011年任团长后，郝井文提议成立“信息作战研究中心”，带领专业骨干展开电磁干扰、信息支援等新型作战样式研究，摸索信息化、流程化、标准化训练规范。" },
        { text: "在当年年底空军首届对抗空战比武中，面对强电磁干扰压制，郝井文灵活运用战法，以大比分优势夺得“金头盔”。" },
        { text: "这次成功尝试，开阔了郝井文的实战化思维。" },
        { text: "2013年以来，郝井文带领团队先后完成空军训练大纲和法规的试验任务，在全空军率先展开海上自由空战、远海超低空飞行、夜间常规武器打地靶、夜间空中加油等多项开创性训练。" },
        { text: "“兵无常势，水无常形。”紧贴实战，更要勇于把握稍纵即逝的“战”机。" },
        { img: imgs.c22, title: "郝井文带领歼击机编队参加空军体系远洋训练（资料照片）。新华社发（陈劼 摄）" },
        { text: "2015年盛夏，部队海训转场当天，天气突变，黑云压顶。" },
        { text: "“我先上去看看情况！”郝井文驾驶战机第一个冲上云霄，发现天气仍有起飞窗口时，立即向塔台报告：“空中有云缝，抓紧定下转场决心！”" },
        { text: "随着指挥员一声令下，十多架战机快速跟进起飞。当最后一架战机突破云层后，机场顿时电闪雷鸣，大雨倾盆。" },
        { text: "这样的魄力和胆识，同样来自于郝井文对“实战”的理解：“战场，不会给我们选择天气的机会！”" },
        { text: "2017年，郝井文率队参加“航空飞镖”国际军事竞赛，尽管取得优异成绩，却依然深受触动。“对方飞行员的每一个动作都有很强的实战意义，这个对我刺激很大。”" },
        { text: "归建后，他及时调整训练思路，要求飞行员在边界条件拉起，瞄准时间和再次攻击时间大大缩短，战场生存率极大提升。" },
        { text: "“实战中，差个5秒，差个10秒，敌人就能把你干掉。”郝井文认为，航空兵平时训练就是要高度关注细节，最后的差距往往就在于此。" },
        { text: "紧贴实战，不仅是在演习场上——即使是平常的一场篮球赛，郝井文也要争第一。飞行员们起初以为“旅长输不起”，后来才知道，在旅长眼里，“战场上没有‘第二’！”" },
        { text: "先辈们写下的“传奇”，在这里延续。2018年，空军新一代军事训练法规试训任务，郝井文所在旅又一次率先完成。" }
    ]

}

var ganyufengxian = { //一心为民
    videoPoster: '',
    videoText: '',
    content: [
        { img: imgs.c31, title: "郝井文准备升空训练（2018年12月14日摄）。新华社发（万全 摄）" },
        { text: "治军先治将。“只有率先垂范，才能影响带动广大党员干部跟着学、照着做，保证党的组织聚焦备战使命、发挥核心作用。”郝井文说。" },
        { text: "翻看这个旅的飞行训练记录，无论是高难课目还是重大任务，郝井文始终是在第一架次、第一梯队，飞在鹰阵最前端。" },
        { text: "随着空军使命任务拓展，郝井文所在部队执行的重大行动任务越来越多，每有任务，他都勇往直前、不辱使命——" },
        { text: "2013年，面对紧急任务，一句“我第一个上，大家跟着”，郝井文就率队紧急升空。那次任务，他组织全负荷大强度出动，连续不间断出动战机，创下6个“首次”。" },
        { text: "2015年，带队警巡东海，面对突发情况，已经返航的郝井文不顾剩余油量紧张，果断掉转机头，指挥6架战机迅速占据有利态势，成功处置，安全返航。" },
        { text: "2016年，他作为歼击机编队长机，奉命掩护轰-6K前出宫古海峡，面对外国军机袭扰，果断出击……" },
        { text: "近年来，郝井文带领部队出色完成钓鱼岛空中维权、东海防空识别区常态化管控和前出西太平洋、飞越第一岛链等重大行动任务，为维护国家主权、安全和发展利益作出重要贡献。" },
        { text: "2017年以来，在空军新一代军事训练大纲和法规试训任务中，郝井文带领飞行员突破以往的飞行“禁区”，将武器装备的作战性能挖潜到极限。" },
        { text: "飞行大队教导员赵瑞说，试训强度之大前所未有，仅飞行员腰上的枪带，就被大过载拉断了十几根。“高强度飞行下来，由于血液往下肢流动，有的飞行员腿上都是血点子。”" },
        { img: imgs.c32, title: "郝井文第一个架次升空执行警巡任务（2018年5月3日摄）。新华社发（黄振伟 摄）" },
        { text: "在郝井文的带领下，这个旅的真打实备思想，一刻未曾懈怠：2017年的最后一个飞行日，是实弹攻击课目；2018年开训，也是实弹攻击课目。" },
        { text: "闻战则喜，英勇顽强。每一次起飞，郝井文和他的团队都做好了随时战斗的准备。" },
        { text: "“旅长最常说的就是‘干！’‘跟我上！’”“金头盔”和“金飞镖”飞行员王立说，“我不上，总得有人上吧？那还是我上吧！”" },
        { text: "愿得此生长报国。在这个旅，官兵们只要在战位一天，必定是战斗的姿态。" },
        { text: "“精神层面，没有比中国军人更强的！”郝井文自信地说，“我们旅的飞行员没有含糊的，只要一声令下，就能升空作战。”" },
        { text: "“祖国、荣誉、责任。”悬挂在这个旅空勤楼大厅的旅训，揭示了郝井文和他所在的这支“王牌”部队的制胜密码。" },
        { text: "“大鹏一日同风起，扶摇直上九万里。”在人民空军加速战略转型的快车道上，对于统帅的“胜战之问”，郝井文和他的团队正在用新时代的新航迹作出回答——" },
        { text: "“敢打必胜，有我无敌！”" }
    ]
}

var comments = [{
    headimgurl: imgs.head,
    name: '',
    content: '',
},
{
    headimgurl: imgs.head,
    name: '',
    content: ''
}


]



var arr = [];


for (var attr in imgs) {
    arr.push(imgs[attr]);
}


var mainImgList = [{
    url: imgs.img1,
    type: 'rect'
}, {
    url: imgs.img2,
    type: 'circle'
}, {
    url: imgs.img3,
    type: 'circle'
}];

var musics = {
    music: {
        src: './assets/music/LuckyDay.mp3',
        autoplay: true,
        name: 'bg',
        loop: true
    },
    photo: {
        src: './assets/music/photo.mp3',
        autoplay: false,
        name: "photo",
        loop: false
    }
};

(function () {

    if (typeof WeixinJSBridge == "object" && typeof WeixinJSBridge.invoke == "function") {
        handleFontSize();
    } else {
        if (document.addEventListener) {
            document.addEventListener("WeixinJSBridgeReady", handleFontSize, false);
        } else if (document.attachEvent) {
            document.attachEvent("WeixinJSBridgeReady", handleFontSize);
            document.attachEvent("onWeixinJSBridgeReady", handleFontSize);
        }
    }

    function handleFontSize() {
        // 设置网页字体为默认大小
        WeixinJSBridge.invoke('setFontSizeCallback', {
            'fontSize': 0
        });
        // 重写设置网页字体大小的事件
        WeixinJSBridge.on('menu:setfont', function () {
            WeixinJSBridge.invoke('setFontSizeCallback', {
                'fontSize': 0
            });
        });
    }
})();