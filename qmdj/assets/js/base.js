var imgs = {
  play: './assets/images/play.png',
  logo: './assets/images/logo2.png',
  indexBg: './assets/images/index-bg1.jpg',
  introduceBg: './assets/images/introduce-bg.jpg',
  detailBg: './assets/images/detail-bg.jpg',
  teamBg: './assets/images/team-bg1.jpg',
  introTitle: './assets/images/intro-title.png',
  friendTitle: './assets/images/friend-title.jpg',
  // detailTitle: './assets/images/detail-title.png',
  // detailTitle1: './assets/images/detail-title2.png',
  // detailTitle2: './assets/images/detail-title3.png',
  entry: './assets/images/entry.png',
  arrow: './assets/images/arrow.png',
  like: './assets/images/like.png',
  point: './assets/images/point.png',
  back: './assets/images/back.png',
  index: './assets/images/index.png',
  videoPoster: "./assets/images/video-poster.png",
  //mask: './assets/images/mask.png',

  msg: './assets/images/msg1.png',
  title1: './assets/images/title1-2.png',
  c1: './assets/images/c1.jpg',
  c12: './assets/images/c12.jpg',
  c2: './assets/images/c2.jpg',
  c22: './assets/images/c22.jpg',
  c3: './assets/images/c3.jpg',
  c32: './assets/images/c32.jpg',
  //head1: './assets/images/head2.jpg',
  head2: './assets/images/head1.jpg',
  heart: './assets/images/heart.jpg',

  head: './assets/images/head.png',
  edit: './assets/images/edit.png',

}

var texts = {
  zlmIntro: '2019年1月25日，中央宣传部向全社会宣传发布其美多吉的先进事迹，授予他“时代楷模”称号。',
  zlmName: '其美多吉',
  zlmIntroContent: [
    '其美多吉是中国邮政集团公司四川省甘孜县邮政分公司邮车驾驶员，承担川藏邮路甘孜到德格段的邮运任务。他爱岗敬业，30年如一日，驾驶邮车在平均海拔3500米的雪线邮路上运送邮件，累计行驶里程140多万公里，没有发生一起责任事故。他意志坚强，遭遇歹徒袭击时挺身而出，用鲜血和生命守护邮件安全，身负重伤后坚持康复锻炼，以坚韧的毅力重新走上工作岗位。他珍爱团结，以螺丝钉精神紧紧钉在川藏线上，将来自党中央的声音、祖国四面八方的邮件送往雪域的各个角落，用真情奉献为促进藏区经济社会发展作出了积极贡献，被群众誉为“雪线邮路的幸福使者”。'
  ],

}

var aigangjingye = {//爱岗敬业
  //title: imgs.detailTitle,
  //videoPoster: imgs.videoPoster,
  //videoUrl: './assets/video/5M.mp4',
  content: [
    { img: imgs.c1, title: '其美多吉与同事在雀儿山下席地而坐吃午餐（2017年8月26日摄）。新华社发（吴光于 摄）' },
    { text: '1954年12月15日，随着川藏公路的开通，两辆崭新的邮政汽车满载着祖国内地发往西藏的上万件邮件，从成都直抵拉萨，开启了川藏干线汽车邮路的历史。半个多世纪过去了，这条沟通西藏与内地联系的邮路，依然是中国邮政通信的主动脉，也是目前全国唯一一条不通火车的一级干线邮路。' },
    { text: '其美多吉是四川甘孜县邮政分公司邮运驾驶组组长，有着一张典型的康巴面孔，有棱有角，肤色黝黑，目光坚毅，头上扎着一条马尾，举手投足间透着潇洒。' },
    { text: '然而，这位看上去有着艺术家风范的康巴男人，却在过去30年里重复做着一件单调而艰苦的事——6000多次往返于甘孜与德格之间，行程140多万公里，约等于绕了赤道35圈，也相当于从地球与月球之间往返了两次。' },
    { img: imgs.c12, title: '其美多吉在中国邮政甘孜县分公司将邮件装车（2017年8月27日摄）。新华社发（吴光于 摄）' },
    { text: '“我小时候，高原上的车很少，在我家乡，第一份报纸是邮车送进来的，第一份中专生的录取通知是邮递员送来的。如果能当上邮车司机，多光荣、多神气啊！”其美多吉说。' },
    { text: '1989年，当这个梦想终于实现时，迎接他的却是一条危险的旅途。209公里的路程即使没有意外也要开上8个小时，中途的雀儿山更是许多司机的噩梦，交通事故频发。' },
    { text: '雪线邮路上，司机们的心目中，多吉的邮车就是航标。特别是“风搅雪”来临，漫天的风雪让人无法分清天空和大地时，多吉的邮车总是在漫漫雪山之上碾出第一道辙。' },
    { text: '但航标都是孤独的。30年来，其美多吉没有在运邮途中吃过一顿正餐。只在家里过过5个除夕，两个孩子出生时，他都在运邮路上。' }
  ]
}
var yongyuchuangxin = { //勇于创新
  //title: imgs.detailTitle1,
  content: [
    { img: imgs.c2, title: "其美多吉出发前与妻子泽仁曲西挥手再见（2016年7月15日摄）。新华社发（周兵 摄）" },
    { text: '多吉的右脸有一道明显的刀疤——那是2012年7月留下的永恒印记。' },
    { text: '当天，他驾驶邮车途经国道318线雅安市天全县境内，行至一陡坡，车速减慢的时候，路边突然冒出12个歹徒，手里挥舞着砍刀、铁棒，将邮车团团围住。' },
    { text: '身中17刀，肋骨被打断4根，头盖骨被掀掉一块，左脚左手静脉被砍断……在进行了8个小时的手术后，他挣扎着捡回一条命。' },
    { text: '然而，由于肌腱断裂，左手难以合拢，别说是开车，生活都难以自理。' },
    { text: '就在其美多吉出事前一年，他即将结婚的大儿子突发心肌梗死，撒手人寰。接连遭遇精神和身体的重创，曾经开朗的多吉变得沉默寡言。' },
    { text: '一系列变故让妻子泽仁曲西流干了眼泪，但面对多吉，她却依然努力微笑，不停地鼓励丈夫，带着他四处求医。' },
    { text: '就在多家医院作出左手几乎不可能完全康复的诊断后，夫妇俩遇到了一位老医生，教给他一套常人难以忍受的“破坏性康复疗法”——先强制弄断僵硬的组织，再让它重新愈合。康复过程痛得钻心，每一次多吉都会疼得满身大汗。通过两个月的咬牙坚持，左手竟然奇迹般地康复了。' },
    { img: imgs.c22, title: '出发前，泽仁曲西将孙子举到丈夫其美多吉跟前，让祖孙俩告别（2017年8月28日摄）。新华社发（吴光于 摄）' },
    { text: '回到甘孜县，同事们都劝他别再开车了，既是心疼他的身体，更怕他再遭遇不测。但妻子知道，面前这个遍体鳞伤的男人，只有重返雪线邮路，才能找回丢失的魂。' },
    { text: '回归车队的那一天，同事为其美多吉献上哈达。他却转身把哈达系上了邮车。他说，行驶在这条路上，能感觉逝去的儿子和曾经的自己又回来了。' },
  ]
}

var ganyufengxian = { //甘于奉献
  //title: imgs.detailTitle2,
  //subTitle: '张黎明在老旧楼区开展志愿服务',
  //videoPoster: imgs.videoPoster,
  //videoText: '电与老百姓的生活休戚相关，张黎明始终惦记着老百姓的用电需要。除了抢修班班长，他还有一个特殊的“头衔”——滨海黎明共产党员服务队队长。  ',
  //videoUrl: './assets/video/ganyufengxian.mp4',
  // imageText: [
  //   '2017年4月12日，滨海新区新开里社区按事先通知安排实施计划停电检修。断电前，服务队突然接到社区居民打来的电话：她96岁的老母亲瘫痪在床，靠呼吸机维持生命，断电随时有生命危险。',
  //   '张黎明叫上队员带上发电机火速出发。在现场，他们架设起20米长的入户供电线路，实施了持续11个小时的特殊供电服务。'
  // ],
  content: [
    { img: imgs.c3, title: '其美多吉与同事驾车行驶在雪线邮路上（资料照片）。新华社发（周兵 摄）' },
    { text: '邮车路过四道班时，停了下来，多吉下车把青菜和肉送到道班工人莫尚伟、黎兴玉夫妇手中。' },
    '这对坚守雀儿山23年的夫妇动情地地讲起他们与多吉的感情。“他是信使，更是亲人。”在荒凉的生命禁区，邮车带着独特节奏的两声鸣笛是他们之间才懂的问候，他送来的报纸和家书更是滋养精神世界的唯一营养。',
    '跟随多吉行驶在雪线邮路上的日子，窗外的风景一直很单调。',
    '然而，从一位位邮政职工、道班工人、汽车司机、交通民警、运管人员的讲述中，记者发现，他的世界一直很精彩。',
    '他与这抹流动的绿，在雪线邮路上架起了一座桥。它连着党中央和藏区的百姓，连着青藏高原和祖国的各族人民。',
    '哪里发生了交通事故，他就成了义务交通员；哪里有了争执摩擦，他就成了人民调解员。',
    '30年来，他带在邮车里的氧气罐、红景天、肌苷口服液，在风雪阻路、进退无路的危难关头，挽救过上百位陌生人的生命。',
    { img: imgs.c32, title: '其美多吉在途中帮助社会车辆安装防滑链（资料照片）。新华社发（周兵 摄）' },
    '如今，全长7公里的雀儿山隧道已经正式通车，隧道将从前两个小时的车程缩短到了10来分钟。',
    '谈起这些时，他的声音里也有些落寞，他说无比怀念雀儿山上那些步步惊心的日子，有他和兄弟们的青春和热血。',
    '那一刻，记者的思绪也被带回了雀儿山上。多吉望着远方无边的草原说，无论道路多么艰险，只要有人在，邮件就会抵达，只要雪线邮路在，这抹流动的绿就将永不消失。'
  ]
}

var comments = [
  {
    headimgurl: imgs.head,
    name: '',
    content: '',
  },
  {
    headimgurl: imgs.head,
    name: '',
    content: ''
  }


]



var arr = [];


for (var attr in imgs) {
  arr.push(imgs[attr]);
}


var mainImgList = [
  {
    url: imgs.img1,
    type: 'rect'
  }, {
    url: imgs.img2,
    type: 'circle'
  }, {
    url: imgs.img3,
    type: 'circle'
  }
];

var musics = {
  music: {
    src: './assets/music/bg2.mp3',
    autoplay: false,
    name: 'bg',
    loop: true
  },
  photo: {
    src: './assets/music/photo.mp3',
    autoplay: false,
    name: "photo",
    loop: false
  }
};

(function () {

  if (typeof WeixinJSBridge == "object" && typeof WeixinJSBridge.invoke == "function") {
    handleFontSize();
  } else {
    if (document.addEventListener) {
      document.addEventListener("WeixinJSBridgeReady", handleFontSize, false);
    } else if (document.attachEvent) {
      document.attachEvent("WeixinJSBridgeReady", handleFontSize);
      document.attachEvent("onWeixinJSBridgeReady", handleFontSize);
    }
  }

  function handleFontSize() {
    // 设置网页字体为默认大小
    WeixinJSBridge.invoke('setFontSizeCallback', {
      'fontSize': 0
    });
    // 重写设置网页字体大小的事件
    WeixinJSBridge.on('menu:setfont', function () {
      WeixinJSBridge.invoke('setFontSizeCallback', {
        'fontSize': 0
      });
    });
  }
})();